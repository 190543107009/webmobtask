package com.wmt.shailjagoswami.activity

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wmt.shailjagoswami.R
import com.wmt.shailjagoswami.adapter.UserListAdapter
import com.wmt.shailjagoswami.entity.UserEntity
import com.wmt.shailjagoswami.model.UserModel
import com.wmt.shailjagoswami.retrofit.APIClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    val DATEFORMAT = "yyyy-MM-dd HH:mm:ss"
    private var adapter: UserListAdapter? = null
    private var list = ArrayList<UserEntity>()
    lateinit var recyclerUserList : RecyclerView

    var page: Int = 1
    var pastVisiblesItems = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var loading = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerUserList= findViewById(R.id.ry_user_list)

        adapter = UserListAdapter(applicationContext, list)
        val layoutManager: LinearLayoutManager =
            LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        recyclerUserList.layoutManager = layoutManager
        recyclerUserList.adapter = adapter
        adapter!!.notifyDataSetChanged()
        if (isNetworkAvailable()) {
            getUserList("1")
        }else{
            Toast.makeText(
                this@MainActivity,
                "Please check Your Internet Connection !!",
                Toast.LENGTH_LONG
            ).show()
        }




        recyclerUserList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) { //check for scroll down
                    visibleItemCount = layoutManager.getChildCount()
                    totalItemCount = layoutManager.getItemCount()
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            if (isNetworkAvailable()) {
                                loading = false
                                page++
                                getUserList(page.toString())
                                loading = true
                            } else {
                                Toast.makeText(
                                    this@MainActivity,
                                    "Please check Your Internet Connection !!",
                                    Toast.LENGTH_LONG
                                ).show()

                            }

                        }
                    }
                }
            }
        })


    }


    private fun getUserList(page: String) {
        val call: Call<UserModel> =
            APIClient.getClient.getUserList(page, "25")
        call.enqueue(object : Callback<UserModel> {

            override fun onResponse(
                call: Call<UserModel>?,
                response: Response<UserModel>?
            ) {
                if (response?.isSuccessful!!) {
                    for (item in response.body()!!.results) {
                        val mObject = UserEntity()
                        mObject.name = item.name.first + " " + item.name.last
                        mObject.email = item.email
                        mObject.imgUrl = item.picture.large
                        mObject.dob = convertUtc2Local(item.dob.date).toString()
                        list.add(mObject)
                    }
                    adapter!!.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<UserModel>?, t: Throwable?) {
                Toast.makeText(applicationContext, "Error " + t?.message, Toast.LENGTH_LONG).show()
                Log.i("msg", "Error " + t?.message)
            }

        })
    }


    fun isNetworkAvailable(): Boolean {
        val connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw      = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                //for other device how are able to connect with Ethernet
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                //for check internet over Bluetooth
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                else -> false
            }
        } else {
            return connectivityManager.activeNetworkInfo?.isConnected ?: false
        }
    }

    fun convertUtc2Local(utcTime: String?): String? {
        var time = ""
        if (utcTime != null) {
            val utcFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
            utcFormatter.timeZone = TimeZone.getTimeZone("UTC")
            var gpsUTCDate: Date? = null //from  ww  w.j  a va 2 s  . c  o  m
            try {
                gpsUTCDate = utcFormatter.parse(utcTime)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val localFormatter = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            localFormatter.timeZone = TimeZone.getDefault()
            assert(gpsUTCDate != null)
            time = localFormatter.format(gpsUTCDate!!.time)
        }
        return time
    }

}