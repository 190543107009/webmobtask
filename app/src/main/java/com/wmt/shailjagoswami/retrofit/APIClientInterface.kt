package com.wmt.shailjagoswami.retrofit

import com.wmt.shailjagoswami.model.UserModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface APIClientInterface {

    @GET("api/?")
    fun getUserList(
        @Query("page") page: String?,
        @Query("results") results: String?
    ): Call<UserModel>
}