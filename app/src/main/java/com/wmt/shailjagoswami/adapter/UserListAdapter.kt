package com.wmt.shailjagoswami.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.wmt.shailjagoswami.R
import com.wmt.shailjagoswami.entity.UserEntity

class UserListAdapter(var context: Context, var list: ArrayList<UserEntity>) :
    RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val Name = itemView.findViewById(R.id.txtUserName) as TextView
        val Email = itemView.findViewById(R.id.txtUserEmail) as TextView
        val DOB = itemView.findViewById(R.id.txtUserDate) as TextView
        val ImgUrl = itemView.findViewById(R.id.imgUser) as ImageView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_user_list, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: UserListAdapter.ViewHolder, position: Int) {
        holder.Name.text = list[position].name
        holder.Email.text = list[position].email
        holder.DOB.text = list[position].dob
        Picasso.with(context).load(list[position].imgUrl)
            .placeholder(R.color.purple_200).into(holder.ImgUrl)
    }

    override fun getItemCount(): Int {
        return list.size
    }

}